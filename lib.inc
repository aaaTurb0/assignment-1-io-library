section .text
 
 
exit: 
    mov rax, 60
    syscall

string_length:
  xor rax, rax
  .loop:
    cmp byte [rdi + rax], 0
    je .end
    inc rax
    jmp .loop
  .end:
    ret

print_string:
  push rdi                
  call string_length
  mov rdx, rax           
  pop rsi               
  mov rdi, 1  
  mov rax, 1    
  syscall
  ret

print_char:
  push rdi
  mov rsi, rsp
  mov rdi, 1
  mov rax, 1
  mov rdx, 1
  syscall
  pop rdi
  ret

print_newline:
  mov rdi, `\n`
  jmp print_char
  

print_uint:
  mov r8, 10
  push rbp
  mov rbp, rsp
  push 0
  mov rsi, rsp
  sub rsp, 20
  mov rax, rdi
  .loop:
    xor rdx, rdx
    div r8
    add dl, '0'
    dec rsi
    mov [rsi], dl
    test rax, rax
    jnz .loop
  mov rdi, rsi
  call print_string
  mov rsp, rbp
  pop rbp
  ret


print_int:
  test   rdi, rdi
  jge   print_uint
  push  rdi
  mov   rdi, `-`
  call  print_char
  pop   rdi
  neg   rdi
  jmp print_uint



string_equals:
  mov rcx, -1
  mov rax, 1
  .loop:
    inc rcx
    mov r8b, byte[rdi+rcx]
    mov r9b, byte[rsi+rcx]
    test r8b, r9b
    jz .ret
    cmp r8b, r9b
    je .loop
  xor rax, rax
  .ret:
    cmp r8b, r9b
    je .end
    xor rax, rax
  .end:
    ret
    


read_char:
  xor rax, rax
  xor rdi, rdi
  sub rsp, 8
  mov rsi, rsp
  mov rdx, 1
  syscall
  test rax, rax
  jz .eof
  mov al, [rsp]
  jmp .end
  .eof:
  xor rax, rax
  .end:
  add rsp,8
  ret

read_word:
  push r12
  push r13
  push r14
  mov r12, rdi
  mov r13, rsi
  xor r14, r14
  .loop:
    push r14
    call read_char
    pop r14       
    cmp rax, ' '
    je .check_to_stop_read
    cmp rax, `\t`
    je .check_to_stop_read
    cmp rax, `\n`
    je .check_to_stop_read
    test rax, rax
    jz .add_null_and_end
    cmp rax, 0x3 
    je .add_null_and_end 
    mov byte [r12 + r14], al
    inc r14
    cmp r14, r13
    je .end
    jmp .loop    
  .check_to_stop_read:
    test r14, r14
    jz .loop   
  .add_null_and_end:
    mov byte [r12 + r14], 0
    mov rax, r12
    mov rdx, r14
    pop r14
    pop r13
    pop r12
    ret
  .end:
    xor rax, rax
    pop r14
    pop r13
    pop r12
    ret




parse_uint:
  xor   rcx, rcx
  xor   rax, rax
  xor   rdx, rdx 
  .loop:
    mov r8b, byte[rdi+rdx]
    cmp r8b, `0`
    jl .exit
    cmp r8b, `9`
    jg .exit
    mov cl, r8b
    sub cl, `0`
    imul rax, 10   
    add rax, rcx
    inc rdx 
    jmp .loop
  .exit:
    ret


parse_int:
  xor   rax, rax
  cmp   byte[rdi], `-`
  jne   parse_uint
  inc   rdi
  call  parse_uint
  neg   rax
  inc   rdx
  ret 



string_copy:
  xor rcx, rcx
  .loop:
    mov al, byte[rdi + rcx]
    inc rcx        
    cmp rcx, rdx         
    jg .fail                
    mov byte[rsi], al         
    inc rsi
    test al, al                
    jz .success
    jmp .loop
  .fail:
    mov rax, 0                     
    jmp .end
  .success:
    mov rax, rcx   
  .end:
    ret
